![Generic badge](https://img.shields.io/badge/State-En_cours_de_création-red.svg)

Deploy Wiki with LXD
================

----------------------------------------

Needed:
+ LXD host already configured, see [install_conf_lxd](https://framagit.org/alban.vidal/install_conf_lxd.git) for more information

----------------------------------------

## Create configuration file for wiki and start deploy

+ Copy `config/example_project` to `config/project_<YOUR_PROJECT_NAME>`
+ Edit values in `config/project_<YOUR_PROJECT_NAME>` file
+ Edit usernames and passwords in `config/project_<YOUR_PROJECT_NAME>` file
+ Start deploy by execute: `./deploy.sh <YOUR_PROJECT_NAME>`
