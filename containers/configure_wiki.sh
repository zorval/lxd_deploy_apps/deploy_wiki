#!/bin/bash

################################################################################

# BSD 3-Clause License
#
# Copyright (c) 2018, Quentin Lejard <zorval@valde.fr>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

################################################################################
##########                    Define color to output:                 ##########
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################

# Path of git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

# Load project vars
PREFIX=$1
source $GIT_PATH/config/project_${PREFIX}

## Nginx vhost

$GIT_PATH/scripts/gen_vhost/gen_vhost.sh $RVPRX_CONTAINER $FQDN $WIKI_IP_PRIV $CLOUD_TECH_ADMIN_EMAIL $CREATE_SSL_CERTIFICATES

echo "$($_GREEN_)~~ BEGIN « wiki » container ~~$($_WHITE_)"

################################################################################
echo "$($_ORANGE_)Install Prerequisites$($_WHITE_)"
lxc exec ${PREFIX} -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install nodejs git mongodb curl > /dev/null
"
################################################################################
# TODO
# Définir le dossier d'installation
echo "$($_ORANGE_) Installation  $($_WHITE_)"
lxc exec ${PREFIX} -- bash -c "
    curl -sSo- https://wiki.js.org/install.sh | bash
"
################################################################################

if [ -n $PORT_WIKI ]
  read -p "Entrez le port sur lequel écoutera le wiki" PORT_DIR
  read -p "Entrez le dossier ou s'installera le wiki" DIR_INSTALL
fi

echo "$($_ORANGE_) Configuration  $($_WHITE_)"
lxc exec ${PREFIX} -- bash -c "
    echo 'Once the installation is completed, you'll be prompted to run the configuration wizard'
    cd ${DIR_INSTALL} && node wiki configure ${PORT_WIKI}
    echo 'Using your web browser, navigate to http://localhost:${PORT_WIKI}/ (replace localhost with the IP of your server / custom port if applicable) and follow the on-screen instructions.'
    echo 'All settings entered during the configuration wizard are saved in the file config.yml. See the Configuration File guide for all the possible options you can use.'

"

echo "Appuyez sur une touche une fois l'installation terminée"
read
################################################################################
echo "$($_ORANGE_) Run Wiki.js  $($_WHITE_)"

lxc exec ${PREFIX} -- bash -c "
    cd ${DIR_INSTALL} && node wiki start
"
