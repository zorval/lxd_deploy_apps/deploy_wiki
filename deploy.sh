#!/bin/bash

################################################################################
# BSD 3-Clause License
#
# Copyright (c) 2018, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
##########                    Define color to output:                 ##########
################################################################################

_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
_BLUE_="tput setaf 4"

################################################################################
##########                         Define path                        ##########
################################################################################
# ./

GIT_PATH="$(realpath ${0%/*})"

################################################################################
##########                      Test si lxd et RVPRX                  ##########
################################################################################

# Exit if LXD is not installed
if ! which lxd >/dev/null;then
    echo "$($_RED_)LXD is not installed$($_WHITE_)"
    echo "Please see https://framagit.org/alban.vidal/install_conf_lxd.git"
    exit 1
fi

# Exit if rvprx is not created
if [ $(lxc ls --format csv rvprx | grep RUNNING | wc -l) -eq 0 ] ; then
    echo "$($_RED_)RVPRX container don't exist or is not started$($_WHITE_)"
    echo "Please see https://framagit.org/alban.vidal/install_conf_lxd.git"
    exit 2
fi

################################################################################
##########                      Test du fichier 00_VARS               ##########
################################################################################

# Ask path of « install_conf_lxd » git directory
if [ -f $GIT_PATH/config/00_VARS ]; then
    source $GIT_PATH/config/00_VARS
else
    echo
    echo "$($_BLUE_)File « $GIT_PATH/config/00_VARS » don't exist$($_WHITE_)"
    echo "(if you need more help, see https://framagit.org/alban.vidal/install_conf_lxd.git)"
    echo
    echo -n "$($_BLUE_)Please enter the path of the « install_conf_lxd » git directory: $($_WHITE_)"
    read GIT_PATH_install_conf_lxd
    echo

    # Test if "GIT_PATH_install_conf_lxd" is correctly set
    if [ ! -d "${GIT_PATH_install_conf_lxd}/config/" ] ; then
        echo "$($_RED_)path of the « install_conf_lxd » git directory is not correct$($_WHITE_)"
        echo "$($_RED_)EXIT$($_WHITE_)"
        exit 99
    fi

    # Wtite to config/00_VARS file
    echo "GIT_PATH_install_conf_lxd='$GIT_PATH_install_conf_lxd'" >> $GIT_PATH/config/00_VARS
fi

################################################################################
##########             Source des variables de base de LXD            ##########
################################################################################

#### Source « install_conf_lxd » conf

# Load Vars
source $GIT_PATH_install_conf_lxd/config/00_VARS

# Load Network Vars
source $GIT_PATH_install_conf_lxd/config/01_NETWORK_VARS

# Load Resources Vars
source $GIT_PATH_install_conf_lxd/config/02_RESOURCES_VARS

# Load Other vars
# - LXD_DEPORTED_DIR
# - DEBIAN_RELEASE
# - LXD_DEFAULT_STORAGE_TYPE
source $GIT_PATH_install_conf_lxd/config/03_OTHER_VARS

################################################################################
##########                     Variables du projet                   ###########
################################################################################

echo

## PROJECT NAME - vars
if [ -z $PROJECT_NAME ] ; then
    echo -n "$($_BLUE_)Please enter a project name: $($_WHITE_)"
    read PROJECT_NAME
    echo
fi

# TODO
# Test project name « ./$\_ »

# Test if config file are present
if [ -f $GIT_PATH/config/project_${PROJECT_NAME} ] ; then
    # Load project vars
    source $GIT_PATH/config/project_${PROJECT_NAME}
else
    echo
    echo "ERROR$($_WHITE_)"
    echo "$($_ORANGE_)config file for this project is absent$($_WHITE_)"
    echo "$($_ORANGE_)Please create '$GIT_PATH/config/project_${PROJECT_NAME}' file$($_WHITE_)"
    echo
    echo "$($_RED_)EXIT$($_WHITE_)"
    exit 3
fi

# Define name of containers
CT_NAME="${PROJECT_NAME}"

################################################################################
##########          Test de la présence du dossier déporté           ###########
################################################################################

# Test directory
if [ -d $LXD_DEPORTED_DIR/${CT_NAME} ] ; then
    echo "$($_RED_)ERROR$($_WHITE_)"
    echo "$($_ORANGE_)Directory '$LXD_DEPORTED_DIR/${CT_NAME}'$($_WHITE_)"
    echo "$($_ORANGE_)Please move or delete it !$($_WHITE_)"
    echo
    echo "$($_RED_)EXIT$($_WHITE_)"
    exit 4
fi

################################################################################
#########      Test de la présence d'un container déja existant      ###########
################################################################################

# Test if the containers is not already exist
if [ $(lxc ls --format csv ${CT_NAME} | wc -l) -ne 0 ] ; then
    echo "$($_RED_)ERROR$($_WHITE_)"
    echo "$($_ORANGE_)Containers already exists !$($_WHITE_)"
    echo
    echo "$($_RED_)EXIT$($_WHITE_)"
    exit 4
fi

################################################################################
#########        CONTAINER CREATION AND NETWORK CONFIGURATION        ###########
################################################################################

# Create container
echo "$($_ORANGE_)Create contaiers - ${CT_NAME}$($_WHITE_)"
lxc launch $LXD_IMAGE_VERSION ${CT_NAME} --profile default --profile privNet

# Test if container if correctly created
if [ $(lxc ls --format csv ${CT_NAME} | wc -l) -ne 1 ] ; then
    echo
    echo "$($_RED_)ERROR$($_WHITE_)"
    echo "$($_ORANGE_)Unable to create container « ${CT_NAME}$($_WHITE_) »"
    echo "$($_RED_)EXIT$($_WHITE_)"
    echo
    exit 5
fi

# Set DNS name server
echo "$($_ORANGE_)Set OpenDNS as resolver - ${CT_NAME}$($_WHITE_)"

# resolv.conf -- see config/vars 'DNS' variable
lxc exec ${CT_NAME} -- bash -c "echo '$DNS' > /etc/resolv.conf"

# Set ip address
echo "$($_ORANGE_)Set IP address - ${CT_NAME}$($_WHITE_)"

# Create dynamic variable for get Pub/Priv IP
THIS_IP_PUB="${CT_NAME^^}_IP_PUB"
THIS_IP_PRIV="${CT_NAME^^}_IP_PRIV"
lxc exec ${CT_NAME} -- bash -c "cat << EOF > /etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address ${!THIS_IP_PUB}${CIDR_PUBLIC}
    gateway $DEFAULT_GW

auto ethPrivate
iface ethPrivate inet static
    address ${!THIS_IP_PRIV}${CIDR_PRIVATE}
EOF"

################################################################################
#########                          REBOOT                            ###########
################################################################################

### Restart container for network configuration
echo "$($_ORANGE_)Restart container$($_WHITE_)"
lxc restart ${CT_NAME} --force

### Wait restart
echo "$($_ORANGE_)wait restart…$($_WHITE_)"
sleep 5

################################################################################
#########                        Dossier déporté                     ###########
################################################################################

echo "$($_ORANGE_)Create and attach deported directory ($LXD_DEPORTED_DIR/…)$($_WHITE_)"

## Wiki
mkdir -p $LXD_DEPORTED_DIR/${PROJECT_NAME}/repo

lxc config device add ${PROJECT_NAME}-${CT_NAME} \
              ${PROJECT_NAME} disk \
              path=/srv/lxd source=$LXD_DEPORTED_DIR/${PROJECT_NAME}/repo

## Set mapped UID and GID to LXD deported directory
echo "$($_ORANGE_)Set mapped UID and GID to LXD deported directory $LXD_DEPORTED_DIR/wp-${PROJECT_NAME}$($_WHITE_)"
chown -R 1000000:1000000 $LXD_DEPORTED_DIR/${PROJECT_NAME}

################################################################################
#########                  Applications conf base                    ###########
################################################################################

### Apply « Basic Debian configuration »

echo "$($_GREEN_)~~ BEGIN Basic Debian configuration « ${CT_NAME} » ~~$($_WHITE_)"

echo "$($_ORANGE_)Basic Debian configuration - ${CT_NAME}$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-get update > /dev/null
    DEBIAN_FRONTEND=noninteractive apt-get -y install $PACKAGES > /dev/null
    DEBIAN_FRONTEND=noninteractive apt-get -y upgrade > /dev/null
    # Basic Debian configuration
    mkdir -p /srv/git
    git clone https://framagit.org/zorval/config_system/basic_config_debian > /dev/null
    # Setup config file for auto configuration
    >                                                /srv/git/basic_config_debian/conf
    echo 'UNATTENDED_EMAIL=\"$TECH_ADMIN_EMAIL\"' >> /srv/git/basic_config_debian/conf
    echo 'GIT_USERNAME=\"$HOSTNAME\"'             >> /srv/git/basic_config_debian/conf
    echo 'GIT_EMAIL=\"root@$HOSTNAME\"'           >> /srv/git/basic_config_debian/conf
    echo 'SSH_EMAIL_ALERT=\"$TECH_ADMIN_EMAIL\"'  >> /srv/git/basic_config_debian/conf
    # Launch auto configuration script
    /srv/git/basic_config_debian/auto_config.sh
"
### smtp container »
echo "$($_ORANGE_)Configure postfix tu use SMTP container ($IP_smtp_PRIV)$($_WHITE_)"
lxc file push $GIT_PATH/templates/all/etc/postfix/main.cf ${CT_NAME}/etc/postfix/main.cf
lxc exec ${CT_NAME} -- bash -c "
    sed -i                                      \
        -e 's/__FQDN__/$FQDN/'                  \
        -e 's/__IP_smtp_PRIV__/$IP_smtp_PRIV/'  \
        /etc/postfix/main.cf
"
################################################################################
#########          Configuration du service du container             ###########
################################################################################

if [ $CONFIG_CT ];then

  echo "$($_ORANGE_)Configure container - ${CT_NAME}$($_WHITE_)"
  $GIT_PATH/containers/configure_wiki.sh {CT_NAME}

  echo "$($_ORANGE_)Clean package cache (.deb files) - ${CT_NAME}$($_WHITE_)"
  lxc exec ${CT_NAME} -- bash -c "
      apt-get clean
  "

  echo "$($_ORANGE_)Reboot container to free memory - ${CT_NAME}$($_WHITE_)"
  lxc restart ${CT_NAME} --force


fi

###############################################################################
#########                      Applications des profils             ###########
###############################################################################

echo "$($_ORANGE_)Set CPU and Memory limits - wiki$($_WHITE_)"
lxc profile add ${CT_NAME} $LXD_PROFILE_MEMORY_wiki
lxc profile add ${CT_NAME} $LXD_PROFILE_CPU_wiki

echo "$($_GREEN_)END$($_WHITE_)"
echo ""
